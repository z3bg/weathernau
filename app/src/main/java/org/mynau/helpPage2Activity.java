package org.mynau;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import org.asdtm.goodweather.R;

public class helpPage2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_page2);

        ImageView background = findViewById(R.id.background);
        background.setOnTouchListener(new OnSwipeTouchListener(helpPage2Activity.this) {
            public void onSwipeRight() {
                finish();
            }
            public void onSwipeLeft() {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), helpPage3Activity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(getString(R.string.token), null);

        if (token != null) {
            finish();
        }
    }
}
