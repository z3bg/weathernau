package org.mynau;

import org.json.JSONException;
import org.json.JSONObject;

public interface AsyncResponse {
    void processFinish(JSONObject output, int resultCode) throws JSONException;
}