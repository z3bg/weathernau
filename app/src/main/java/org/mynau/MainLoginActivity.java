package org.mynau;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import org.asdtm.goodweather.R;

public class MainLoginActivity extends AppCompatActivity implements AsyncResponse, GoogleApiClient.OnConnectionFailedListener {

    private LoginButton loginButton;
    private CallbackManager callbackManager;
    public AsyncResponse FBdelegate = null;
    RelativeLayout loader;
    TextView result;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 123;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private GoogleApiClient mGoogleApiClient;

    CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main_login);

        sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        String token = sharedPreferences.getString(getString(R.string.token), null);
        Log.v("myNau", "MainLoginActivity: " + token);
        if (token != null) {
            // Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
            loadUserPanel(token);
        } else {

            callbackManager = CallbackManager.Factory.create();

            ImageView background = findViewById(R.id.background);
            background.setOnTouchListener(new OnSwipeTouchListener(MainLoginActivity.this) {
                public void onSwipeRight() {
                }

                public void onSwipeLeft() {
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), helpPage2Activity.class);
                    startActivity(intent);
                }

            });

            Button clickButton = findViewById(R.id.password_login_button);
            clickButton.setOnClickListener(v -> {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            });

            result = findViewById(R.id.result);
            loader = findViewById(R.id.loader);

            mCallbackManager = CallbackManager.Factory.create();
            loginButton = findViewById(R.id.login_button);
            loginButton.setReadPermissions("email");
            FBdelegate = this;
            // Callback registration
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(final LoginResult loginResult) {
                    loader.setVisibility(View.VISIBLE);
                    // App code
                    Log.e("myNau", "retornou sucesso");
                    Api.doFacebookLogin FBlogin = new Api.doFacebookLogin();
                    FBlogin.delegate = FBdelegate;
                    FBlogin.execute(loginResult.getAccessToken().getToken(), loginResult.getAccessToken().getUserId());
                }

                @Override
                public void onCancel() {
                    // App code
                    Log.e("myNau", "retornou cancel");
                }

                @Override
                public void onError(FacebookException exception) {
                    // App code
                    Log.e("myNau", exception.toString());
                }
            });

            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            if (accessToken != null) {
                loader.setVisibility(View.VISIBLE);
                Api.doFacebookLogin FBlogin = new Api.doFacebookLogin();
                FBlogin.delegate = FBdelegate;
                FBlogin.execute(accessToken.getToken(), accessToken.getUserId());
            }

            configureSignIn();

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
            if (account != null) {
                loader.setVisibility(View.VISIBLE);
                Api.doGoogleLogin GoogleLogin = new Api.doGoogleLogin();
                GoogleLogin.delegate = FBdelegate;

                GoogleLogin.execute(account.getEmail(), account.getId(), account.getDisplayName(), account.getPhotoUrl().toString());
            }

            SignInButton signInButton = findViewById(R.id.sign_in_button);
            signInButton.setSize(SignInButton.SIZE_STANDARD);
            findViewById(R.id.sign_in_button).setOnClickListener(v -> {
                signIn();
            });

        }
    }

    private void configureSignIn() {
        // Configure sign-in to request the user’s basic profile like name and email
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(…);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, save Token and a state then authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                assert account != null;
                String id = account.getId();
                String name = account.getDisplayName();
                String email = account.getEmail();
                String photo = (account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : null);

                loader.setVisibility(View.VISIBLE);
                Api.doGoogleLogin GoogleLogin = new Api.doGoogleLogin();
                GoogleLogin.delegate = FBdelegate;
                GoogleLogin.execute(email, id, name, photo);
            } else {
                // Google Sign In failed, update UI appropriately
                Log.e("myNau", "Login Unsuccessful.");
                Log.e("myNau", result.getStatus().toString());
                Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public void loadUserPanel(String token) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), UserPanelActivity.class);
        intent.putExtra("token", token);
        startActivity(intent);
        finish();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void processFinish(JSONObject output, int resultCode) {
        try {
            if (output != null) {
                if (output.getBoolean("success")) {
                    JSONObject data = (JSONObject) output.get("data");

                    String token = data.getString("token");

                    editor.putString(getString(R.string.token), token);
                    editor.apply();

                    loadUserPanel(token);
                } else {
                    String message = output.getString("error");
                    result.setText(message);
                    loader.setVisibility(View.GONE);
                }
            } else {
                result.setText("Your login is not valid, please try again!");
                LoginManager.getInstance().logOut();
                loader.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // result.setText("An error ocurred, pleae try again in a few seconds... if the problem persist please contact us through our website nau.mobi");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(getString(R.string.token), null);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
